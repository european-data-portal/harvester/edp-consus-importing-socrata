package io.piveau.consus;

public final class ApplicationConfig {

    public static final String ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY = "PIVEAU_IMPORTING_SEND_LIST_DELAY";
    public static final Integer DEFAULT_PIVEAU_IMPORTING_SEND_LIST_DELAY = 8000;

    public static final String ENV_PIVEAU_PAGE_SIZE = "PIVEAU_PAGE_SIZE";
    public static final Integer DEFAULT_PIVEAU_PAGE_SIZE = 1000;

    public static final String ENV_SOCRATA_API_TOKEN = "SOCRATA_API_TOKEN";
}
